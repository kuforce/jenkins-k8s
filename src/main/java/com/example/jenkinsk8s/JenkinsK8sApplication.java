package com.example.jenkinsk8s;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsK8sApplication {

	public static void main(String[] args) {
		SpringApplication.run(JenkinsK8sApplication.class, args);
	}
}
